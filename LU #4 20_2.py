#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Feb 27 13:10:40 2018

@author: Toto
"""

#tuple and dictionaries

my_tuple=(1,4,6,9,2,5,5)
my_tuple[1]
print(type(my_tuple), my_tuple)
my_tuple[0]
my_tuple[0:4]

my_list=[23,4,5,6,8,9,23,4,5,6,7]
print(type(my_list),my_list)
my_list[0]
my_list[2:6]

my_list.append(89)
my_list
my_list[0]
my_list.sort()
my_list.sort(reverse=True)

stock_prices={'AAPL':100}
print(type(stock_prices), stock_prices)
stock_prices['AAPL']

'we use dictionaries to associate, key/pair values without having to create a variable
'for each one of them

stock_prices={'AAPL':45,
             'GOOG':89,
             'XOM':78}

stock_prices.values()
stock_prices.keys()
stock_prices.items()

stock_prices['AAPL']=120
'to update the values

'''Tuple
■ When you need to have a collection of things and don’t need to change it
○ List
■ When you need a collection of things and want to be able to change it
○ Dictionary
■ When you need to associate two things using a key-value store'''

#1
tuplino=("a","b","c")
tuplino[2]

#2
tup=(1,2,3)
tup[0]=5
'tuple object does not support item assignment'

#3
tuplino.sort()
'tuple object has no attribute sort'

#4


#5
'Apart from tuples being immutable there is also a semantic distinction that 
'should guide their usage. Tuples are heterogeneous data structures (i.e., their
'entries have different meanings), while lists are homogeneous sequences. 
'Tuples have structure, lists have order.

'''
Similarities:

Duplicates - Both tuples and lists allow for duplicates
Indexing, Selecting, & Slicing - Both tuples and lists index using integer values 
found within brackets. So, if you want the first 3 values of a given list or tuple,
 the syntax would be the same

Comparing & Sorting - Two tuples or two lists are both compared by their first 
element, and if there is a tie, then by the second element, and so on. No further
attention is paid to subsequent elements after earlier elements show a difference.

Differences: 

Syntax - Lists use [], tuples use ()

Mutability - Elements in a given list are mutable, elements in a given tuple are NOT mutable.



# Tuples are NOT mutable:       
Homo vs. Heterogeneity of Elements - Generally list objects are homogenous and 
tuple objects are heterogeneous. That is, lists are used for objects/subjects 
of the same type (like all presidential candidates, or all songs, or all runners)
 whereas although it's not forced by), whereas tuples are more for heterogenous objects.

Looping vs. Structures - Although both allow for looping (for x in my_list...),
 it only really makes sense to do it for a list. Tuples are more appropriate 
 for structuring and presenting information (%s %s residing in %s is an %s and 
 presently %s % ("John","Wayne",90210, "Actor","Dead"))
 
 




















